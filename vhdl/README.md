# VHDL examples

Here you are some VHDL examples ready to run using GHDL (`bash run.py`).
You can also take the `.vhdl` files to use the Vivado simulator.

These were companion examples of the following presentations:
* [VHDL for Synthesis](http://indico.ictp.it/event/9644/session/2/contribution/11/material/slides/0.pdf)
* [VHDL for Simulation](http://indico.ictp.it/event/9644/session/3/contribution/14/material/slides/0.pdf)

More about GHDL (a free and open-source VHDL simulator):
* [Repository](https://github.com/ghdl/ghdl)
* [Quick Start Guide](https://ghdl.github.io/ghdl/quick_start/index.html)
* [Docker Containers](https://github.com/ghdl/docker)
* [Windows package](https://github.com/hdl/MINGW-packages) (and other FOSS tools).
